<?php
    if(isset($_POST["name"]) && isset($_POST["email"])){

        $name=filter_input(INPUT_POST, "name", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        require 'db_connection.php';
        $conn=connectDB();
        
        $send = $conn->prepare('INSERT INTO charity (name, email) VALUES (?, ?)');
        $send->bind_param("ss", $name, $email);

        if($send->execute()=== TRUE){
            echo "New Charity was saved";
            header("Refresh: 2; URL=view.php");
        } else {
            echo "Error: " . $send->error;
        }
        $send->close();
        $conn->close();
    } else {
        echo "Fill all the fields.";
    }
?>
