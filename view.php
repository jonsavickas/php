<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body{
            background-color: whitesmoke;
        }
        table{
            width: 25%;
            border: black solid 1px;
        }

    </style>
</head>
<body>
    <?php
    require 'db_connection.php';
    $conn = connectDB();
    $sql = 'SELECT * FROM charity';
    $result = $conn->query($sql);
    if($result->num_rows>0){
    ?>
    <table>
        <tr>
            <td>Charity ID</td>
            <td>Name</td>
            <td>Email address</td>
        </tr>
        
        <?php
        while ($row = $result -> fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $row['id'] ?></td>
                <td><?php echo $row['name'] ?></td>
                <td><?php echo $row['email'] ?></td>
                <td>
                    <a href="edit_charity.php?od=<?php echo $row['id'];?>">Edit Charity</a>
                    <a href="delete_charity.php?id=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure that you want to delete this charity')">Delete Charity</a>
                </td>
            </tr>
         
        <?php 
        }
        ?>
    </table>
    <a href="add_charity.php" onclick="">Add new Charity</a>

    <?php
    } else{
        echo 'There were no entries found';
    }
    ?>
    <?php
   
    $sql = 'SELECT * FROM donation';
    $result = $conn->query($sql);
    if($result->num_rows>0){
    ?>
    <table>
        <tr>
            <td>Dontation ID</td>
            <td>Donor name</td>
            <td>Amount</td>
            <td>Charity ID</td>
            <td>date</td>
        </tr>
        
        <?php
        while ($row = $result -> fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $row['id'] ?></td>
                <td><?php echo $row['donor_name'] ?></td>
                <td><?php echo $row['amount'] ?></td>
                <td><?php echo $row['charityID'] ?></td>
                <td><?php echo $row['date'] ?></td>

            </tr>
         
        <?php 
        }
        ?>
    </table>
    <a href="add_donation.php" onclick="">Add new Donation</a>

    <?php
    } else{
        echo 'There were no entries found';
    }
    $conn -> close();
    ?>
    
</body>
</html>