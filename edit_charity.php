<?php
    require 'db_connection.php';
    if (isset($_GET['id']) && is_numeric($_GET['id'])){
        $id=$_GET['id'];
        $conn= connectDB();

        $sql="SELECT * FROM charity WHERE id = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        if($result->num_rows > 0){
            $charity= $result->fetch_assoc();
            ?>

            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>
            </head>
            <body>
                <form action="update_charity.php" method="get">
                    <input type="hidden" name="id" value="<?php echo $charity ['id'] ?>"/>
                    Name: <input type="text" name="name" value="<?php echo $charity ['name'] ?>"/>
                    <br>
                    Email: <input type="email" name="email" value="<?php echo $charity['email']?>"/>
                    <br>
                    <input type="submit" value="save">
                </form>
            </body>
            </html>

            <?php
        } else{
            echo "Update couldnt be saved, error: ". $stmt->error;
        }
        $conn->close();
        $stmt->close();
    } 
    else {
        echo "This entry does not exist. ";
    }
?>