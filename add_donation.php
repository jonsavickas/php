<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="insert_donation.php" method="post">
        <label for="donor_name">Donors name: </label>
        <input type="text" name="donor_name" required>
        <br>
        <label for="amount">Amount: </label>
        <input type="number" name="amount" required>
        <br>
        <label for="charityID">Charity ID: </label>
        <select name="charityID" required>
            <option value="">Select Charity</option>
            <?php
        require 'db_connection.php';
        $conn=connectDB();
        $sql = "SELECT id FROM charity";
        $result = $conn->query($sql);

        while($row = $result->fetch_assoc()){
            echo "<option value='{$row['id']}'>{$row['name']}</option>";        
        }
    
        $conn->close();
        ?>
        </select>
        <br>
        <label for="date"></label>
        <input type="date" name="date" required>
        <br>
        <input type="submit" value="save">
    </form>
    
</body>
</html>