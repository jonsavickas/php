<?php
    if(isset($_POST["donor_name"]) && isset($_POST["amount"]) && isset($_POST["charityID"]) && isset($_POST["date"])){

        $donor_name=filter_input(INPUT_POST, "donor_name", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $amount = filter_input(INPUT_POST, "amount", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $charityID = filter_input(INPUT_POST, "charityID", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $date = filter_input(INPUT_POST, "date", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        require 'db_connection.php';
        $conn=connectDB();
        
        $send = $conn->prepare('INSERT INTO donation (donor_name, amount, charityID, date) VALUES (?, ?, ?, ?)');
        $send->bind_param("ssss", $donor_name, $amount, $charityID, $date);

        if($send->execute()=== TRUE){
            echo "New Donation was saved";
            header("Refresh: 2; URL=view.php");
        } else {
            echo "Error: " . $send->error;
        }
        $send->close();
        $conn->close();
    } else {
        echo "Fill all the fields.";
    }
?>