<?php
    require "db_connection.php";
    if($_SERVER['REQUEST_METHOD'] === "POST"){
        $id=$_POST['id'];
        $name=filter_input(INPUT_POST, "name", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        $conn=connectDB();

        $sql = "UPDATE charity SET name = ?, email = ? WHERE id = ? ";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ssi", $name, $email, $id);

        if($stmt->execute()){
            header("location: view.php");
        } else {
            echo "unable to update charity, error: " .$stmt->error;
        }
        $conn->close();
        $stmt->close();
    }
?>