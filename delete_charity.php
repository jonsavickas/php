<?php
     if (isset($_GET['id']) && is_numeric($_GET['id'])){
        $id=$_GET['id'];
        require "db_connection.php";
        $conn = connectDB();

        $sql="DELETE FROM charity WHERE id = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('i', $id);

        if($stmt->execute()){
            header("Location: view.php");
        } else {
            echo "Unable to delete this Charity, error: " . $stmt->error;
        }
     }
     else {
        echo "This entry doesnt exist.";
     }
?>
